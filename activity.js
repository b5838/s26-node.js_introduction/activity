// 1. What directive is used by Node.js in loading the modules it needs?

        "Answer: require"

// 2. What Node.js module contains a method for server creation?

        "Answer: HTTP module"

// 3. What is the method of the http object responsible for creating a server using Node.js

        "Answer: createServer"

// 4. What method of the response object allows us to set status codes and content types?

        "Answer: writeHead"

// 5. Where will console.log() output it's contents when run in Node.js

        "Answer: Terminal"

// 6. What property of the request object contains the address's endpoint?

        "Answer: request.url"

// 7. Create aserver

        const http = require("http");

        http.createServer((request,response) => {
            if(request.url == "/login"){
                response.writeHead(200, {"Content-Type":"text/plain"});
                response.end("Welcome to the login page")
            }
            else{
                response.writeHead(404, {"Content-Type":"text/plain"});
                response.end("I'm sorry the page you are looking for cannot be found.")
            }
        }).listen(3000);

        console.log(`Server runs successfully at localhost:3000`)